# include .env.local

# TASKS = check test release clean
# https://chriswiegman.com/2021/08/using-parameters-in-a-make-target/
# .PHONY: $(TASKS)
# $(TASKS):
# 	echo $@

# # Executables (local)
# DOCKER_COMP = docker compose

# # Docker containers
# PHP_CONT = $(DOCKER_COMP) exec php

# # Executables
# PHP      = $(PHP_CONT) php
# COMPOSER = $(PHP_CONT) composer
# SYMFONY  = $(PHP_CONT) bin/console

# # Misc
# .DEFAULT_GOAL = help
# .PHONY        = help build up start down logs sh composer vendor sf cc

# .PHONY: clean
# clean:
# 	bin/console cache:clear --env=$(env)

# https://www.strangebuzz.com/en/snippets/the-perfect-makefile-for-symfony
# —— Inspired by ———————————————————————————————————————————————————————————————
# http://fabien.potencier.org/symfony4-best-practices.html
# https://speakerdeck.com/mykiwi/outils-pour-ameliorer-la-vie-des-developpeurs-symfony?slide=47
# https://blog.theodo.fr/2018/05/why-you-need-a-makefile-on-your-project/
# Setup ————————————————————————————————————————————————————————————————————————

# Parameters
WORKING_BRANCH     = devel
DEFAULT_BRANCH     = main
DEPLOY_BRANCH     = Production
REMOTE     = origin

# Executables
EXEC_PHP      = php
COMPOSER      = composer
REDIS         = redis-cli
GIT           = git
YARN          = yarn
NPX           = npx

# Alias
SYMFONY       = $(EXEC_PHP) -d memory_limit=-1 bin/console
# if you use Docker you can replace with: "docker-compose exec my_php_container $(EXEC_PHP) bin/console"

# Executables: vendors
PHPUNIT       = ./vendor/bin/phpunit
PHPSTAN       = ./vendor/bin/phpstan
PHP_CS_FIXER  = ./vendor/bin/php-cs-fixer
PHPMETRICS    = ./vendor/bin/phpmetrics

# Executables: local only
SYMFONY_BIN   = symfony
# BREW          = brew
DOCKER        = docker
DOCKER_COMP   = docker-compose

# Executables: prod only
CERTBOT       = certbot


# Misc
.DEFAULT_GOAL = help
.PHONY        : # Not needed here, but you can put your all your targets to be sure
                # there is no name conflict between your files and your targets.

## —— 🐝 The Strangebuzz Symfony Makefile 🐝 ———————————————————————————————————
help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'


## —— Git

# build:
# 	bin/console assets:install
# 	sed -i "s/APP_VERSION=.*/APP_VERSION=v`date +%Y.%m.%d`/" .env
# 	if [ -f .env.local ]; then sed -i "s/APP_VERSION=.*/APP_VERSION=v`date +%Y.%m.%d`/" .env.local; fi
# 	yarn run encore production
# 	git add -A && (git commit -m "make build" || true)

# git-pull:  ## git pull 
# 	git pull $(REMOTE) 
# git-push: ##   ## git push
# 	git pull $(REMOTE)
init: ##   ## git fetch remote
	@git fetch $(REMOTE)
	git checkout Production && git pull
	git checkout Pre-Production && git pull
	git checkout main && git pull
	git checkout devel && git pull

merged: ## git merge working branch into default branch
	git checkout $(DEFAULT_BRANCH)
	git pull $(REMOTE) $(DEFAULT_BRANCH)
	if [ $(WORKING_BRANCH) != $(DEFAULT_BRANCH) ]; then git merge $(WORKING_BRANCH); fi
	git checkout $(WORKING_BRANCH) && git merge $(DEFAULT_BRANCH)
	git pull $(REMOTE) $(DEFAULT_BRANCH)
	git push $(REMOTE) $(DEFAULT_BRANCH) $(WORKING_BRANCH)

daytag: merge ## Merge working branch into default branch and build tag of day
	git checkout $(DEPLOY_BRANCH)
	git merge $(DEFAULT_BRANCH)
	git tag v`date +%Y.%m.%d` --delete || true
	git push $(REMOTE) v`date +%Y.%m.%d` --delete || true
	git tag v`date +%Y.%m.%d`
	git push $(REMOTE) $(DEFAULT_BRANCH) $(DEPLOY_BRANCH) $(WORKING_BRANCH) v`date +%Y.%m.%d`
	git checkout $(WORKING_BRANCH)

# build:
# 	bin/console assets:install
# 	sed -i "s/APP_VERSION=.*/APP_VERSION=v`date +%Y.%m.%d`/" .env
# 	if [ -f .env.local ]; then sed -i "s/APP_VERSION=.*/APP_VERSION=v`date +%Y.%m.%d`/" .env.local; fi
# 	yarn run encore production
# 	git add -A && (git commit -m "make build" || true)



## —— Composer 🧙‍♂️ ————————————————————————————————————————————————————————————
composer-install: composer.lock ## Install vendors according to the current composer.lock file
	$(COMPOSER) install --no-progress --prefer-dist --optimize-autoloader

composer-update:  ## Update vendors
	$(COMPOSER) update --no-progress --prefer-dist --optimize-autoloader
	$(SYMFONY) c:c
	$(SYMFONY) assets:install
	


## —— Symfony 🎵 ———————————————————————————————————————————————————————————————
sf: ## List all Symfony commands
	$(SYMFONY)

cc: ## Clear the cache. DID YOU CLEAR YOUR CACHE????
	$(SYMFONY) c:c

warmup: ## Warmup the cache
	$(SYMFONY) cache:warmup

fix-var-perms: ## Fix permissions of all var files
	@sudo chmod -R 777 var

assets: ## Install the assets with symlinks in the public folder
	$(SYMFONY) assets:install

purge: ## Purge cache and logs
	@sudo rm -rf var/cache/* var/logs/*




