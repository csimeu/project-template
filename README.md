# Project template

Starter Pack pour organiser et gérer un projet avec GitLab 🦊

Ce dépot à pour objectif de fournir un outil de démarrage rapide pour organiser et gérer un nouveau projet avec GitLab.

Partant du constat que le démarrage d'un projet est un processus long et complexe, j'ai voulu fournir un outil simple et efficace pour lancer un projet avec GitLab et ses outils !

##  1. <a name='Queproposecedpt'></a>Que propose ce dépôt ?

Ce dépôt fournit un ensemble de modèles, de fichiers et de paramétrages pour vous faciliter le démarrage d'un projet avec GitLab, vous y retrouverez les éléments suivants :

-   Ce fichier README.md
-   Des modèles pour les issues et les merges requests
-   Une collection de labels
-   Un modèle de Board
-   3 Branches :
    -   Main
    -   Pré-Production
    -   Production

##  2. <a name='Commentutilisercedpt'></a>Comment utiliser ce dépôt ?

> Vous pouvez utiliser ce dépôt comme **source d'inspiration** pour votre animer votre projet avec GitLab simplement en copiant et adaptant les idées que vous trouverez pertinentes dans votre contexte.

##  5. <a name='Licence'></a>Licence

Ce dépôt est sous licence [MIT](LICENSE)

##  6. <a name='Auteur'></a>Auteur

[L2dw](https://l2dw.space)